from flask import Flask,render_template
app = Flask(__name__)

@app.route('/')
def index():
    return render_template("index.html")

@app.route('/mission')
def mission():
    return render_template("mission/index.html")


@app.route('/intro')
def intro():
    return render_template("intro/index.html")

@app.route('/current')
def current():
    return render_template("current/index.html")

@app.route('/funding')
def funding():
    return render_template("funding/index.html")


@app.route('/gallery')
def gallery():
    return render_template("gallery/index.html")

if __name__ == '__main__':
    app.run(debug=True,host="0.0.0.0",port="8000" )

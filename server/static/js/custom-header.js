const template = document.createElement('template');
template.innerHTML = `
<header>
<meta name="viewport" content="width=1100,  shrink-to-fit=no">
    <nav class="navbar navbar-expand-lg navbar-light shadow-sm">
    <div class="container">
        <a class="navbar-brand" href="/"><img src="/static/img/logo.png"></a>
        <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupport">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item pointer">
                <a class="nav-link" href="/mission ">미션</a>             
            </li>
            <li class="nav-item pointer">
                <a class="nav-link" href="/current">장학금 지급 현황</a>             
            </li>
            <li class="nav-item pointer">
                <a class="nav-link" href="/funding">기부금 활용 내역</a>             
            </li>
            <li class="nav-item pointer">
                <a class="nav-link" href="/gallery">갤러리</a>             
            </li>
        </ul>
        </div> <!-- .navbar-collapse -->
    </div> <!-- .container -->
    </nav>
</header>
<style>
    .pointer{
        cursor:pointer;
    }
    ul{
        margin-top:0px;
        margin-bottom: 0px;
        padding-left:0px;  
    }
    li{
        list-style:none;
    }
    header{
        width: 100%;
    }
    .navbar-brand > img{
        width: 250px;
        margin-top:10px;
    }
    .container{
        width: 100%;
        max-width: 1140px;
        margin: 0px auto;
        display:flex;
        align-items:center;
        justify-content: space-between;
        height:90px;
    }
    .navbar {
        height:90px;
        min-height: 90px;
        position: relative;    
        display: flex;
        -ms-flex-wrap: wrap;    
        align-items: center;     
        justify-content: space-between;
        padding: 0rem 1rem;
    }
    .shadow-sm {
        box-shadow: 0 0.125rem 0.25rem rgba(0,0,0,0.075)!important;
    }
    .navbar-nav{
        display:flex;
    }
    .nav-link{
        font-weight:bold;
    }
    .ml-auto, .mx-auto {
        margin-left: auto!important;
    }
    a{
        text-decoration:none;
        color:inherit;
    }
    ul.navbar-nav{
        height:90px;
    }
    li.nav-item{
        line-height: 90px;
        list-style: none;
        padding: 0px 20px;
    }
    .hidden{
        display:none;
    }
    .nav-item:hover>.hidden{
        display:flex;
    }
    .nav-item:hover>.sub_category{
        border-top: 2px solid #ddd;
        z-index:1000;
        position:absolute;
        width: 150px;
        padding-top: 0px;
        background-color: #fff;
        display:flex;
        flex-direction:column;
    }
    .sub_category>li{
        height: 60px;
        line-height:60px;
        padding: 0px 20px;
    }
    .sub_category>li:hover{
        background-color:#d74c47;
        color:#fff;
    }
</style>
`

class CustomHeader extends HTMLElement {
    constructor() {
        super();
        this.attachShadow({ mode: 'open' });
        this.shadowRoot.appendChild(template.content.cloneNode(true));
    }

    connectedCallback() {
        this.render();
    }

    render() {

    }
}
window.customElements.define('custom-header', CustomHeader);
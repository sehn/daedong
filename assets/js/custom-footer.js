const footer_template = document.createElement('template');
footer_template.innerHTML = `
<div style="width:100%; background-color:#fff !important; display:flex; justify-content:center" >
    <img onclick="window.open('https://www.nts.go.kr/')" src="/static/img/logo_footer_1.jpg" style="height: 40px;margin: 10px 40px;; cursor:pointer">
    <img onclick="window.open('https://www.acrc.go.kr/acrc/index.do')" src="/static/img/logo_footer_0.jpg" style="height: 40px;margin: 10px 40px; cursor:pointer">
    <img onclick="window.open('https://www.pen.go.kr/')" src="/static/img/logo_boosan.png" style="height: 40px;margin: 10px 40px; cursor:pointer">
</div>
<footer class="page-footer" >
    <div class="container">
        <div class="row " style="display:flex;padding-left: 15px;">

           <span>주소 : 부산시 동구 중앙대로 236번길 3-4 대동빌딩 7층 | TEL : 051-467-7530 | FAX : 051-467-7530
        </div>
        <hr>
        <p id="copyright">Copyright &copy; 2021 DAEDONG SCHOLARSHIP FOUNDATION. All right reserved</p>
    </div>
</footer>
<style>
    *{
        box-sizing: border-box;
    }
    a{
        text-decoration:none;
        color:inherit;
    }
    .pointer{
        cursor:pointer;
    }
    ul{
        margin-top:0px;
        margin-bottom: 0px;
        padding-left:0px;  
    }
    li{
        list-style:none;
    }
    .container{
        padding-right: 15px;
        padding-left: 15px;
        margin-right: auto;
        margin-left: auto;
        max-width: 1140px;
        width:100%
    }
    .row {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        margin-right: -15px;
        margin-left: -15px;
    }    
    .page-footer {
        position: relative;
        display: block;
        padding-top: 40px;
        padding-bottom: 16px;
        background-color: #2D3B38;
        color: #fff;
        margin-top: 30px;
    }
    .page-footer h5 {
        font-weight: 500;
        margin-bottom: 16px;
    }
    .pl-md-3, .px-md-3 {
        padding-left: 1rem !important;
    }
    .pb-3, .py-3 {
        padding-bottom: 1rem !important;
    }
    .pt-3, .py-3 {
        padding-top: 1rem !important;
    }
    .col-lg-3 {
        -ms-flex: 0 0 25%;
        flex: 0 0 25%;
        max-width: 25%;
    }
    .col-1, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12, .col, .col-auto, .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm, .col-sm-auto, .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12, .col-md, .col-md-auto, .col-lg-1, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg, .col-lg-auto, .col-xl-1, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl, .col-xl-auto {
        position: relative;
        width: 100%;
        padding-right: 15px;
        padding-left: 15px;
    }
    .col-4{
        width: 33.33%
    }
    h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
        margin-bottom: 0.5rem;
        font-weight: 500;
        line-height: 1.2;
    }
    .page-footer h5 {
        font-weight: 500;
        margin-bottom: 16px;
    }
    h5, .h5 {
        font-size: 1.25rem;
    }
    .footer-menu a, .footer-link {
        display: inline-block;
        padding-top: 6px;
        padding-bottom: 6px;
        color: rgba(255, 255, 255, 0.6);
        transition: all .2s ease;
    }
    .footer-link {
        padding: 0;
        margin-bottom: 16px;
    }
    .mt-2, .my-2 {
        margin-top: 0.5rem !important;
    }
    @media (min-width: 992px)
    .container, .container-sm, .container-md, .container-lg {
        max-width: 960px;
    }
</style>
`

class CustomFooter extends HTMLElement {
    constructor() {
        super();
        this.attachShadow({ mode: 'open' });
        this.shadowRoot.appendChild(footer_template.content.cloneNode(true));
    }

    connectedCallback() {
        this.render();
    }

    render() {

    }
}
window.customElements.define('custom-footer', CustomFooter);